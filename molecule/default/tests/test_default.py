import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_samba_interfaces(host):
    file = host.file("/etc/samba/interfaces.conf")
    assert file.contains("interfaces = eth0")


def test_route_interfaces(host):
    file = host.file("/etc/sysconfig/network-scripts/route-eth0")
    assert file.contains("172.30.0.0")


def test_route_exist(host):
    cmd = host.run("/usr/sbin/ip route list")
    assert "172.30.0.0" in cmd.stdout


def test_sudoers_files(host):
    with host.sudo():
        assert host.file("/etc/sudoers.d/csi").content_string.strip() == "csi ALL=(ALL) NOPASSWD:ALL"


def test_sudo_with_csi(host):
    with host.sudo("csi"):
        cmd = host.run("sudo ls")
    assert cmd.rc == 0
