# ics-ans-gpn-config

Ansible playbook to configure servers on the GPN:

- configure samba so that the interfaces created by docker are not broadcasted to AD and DNS
- add static route to reach cslab networks
- add csi user to servers


This playbook should be applied on all nodes on the GPN (and only on the GPN).

WARNING! Be sure to run this playbook on the proper list of nodes!


## License

BSD 2-clause
